// eslint-disable-next-line import/extensions
import grades from '../data/GradesToAllowance.json';

/**
 * Parses the assignment data and returns object with
 * summary
 *
 * @param {Array} assignments
 */
export function computeLetterGrade(assignments) {
	let total = 0;
	let totalPossible = 0;
	assignments.forEach(assignment => {
		if (!isNaN(parseFloat(assignment.score))) {
			totalPossible += parseFloat(assignment.pointspossible);
			total += parseInt(assignment.score, 0);
		}
	});
	const pointsToC = Math.round((0.7 * totalPossible) - total);

	return {
		total,
		possible: totalPossible,
		percent: total / totalPossible,
		pointsToC: (pointsToC >= 0) ? pointsToC : '',
	};
}

export function percentToLetterGrade(percent) {
	if (percent <= 0.58) {
		return 'F';
	}
	if (percent <= 0.59) {
		return 'F+';
	}
	if (percent <= 0.60) {
		return 'D-';
	}
	if (percent <= 0.68) {
		return 'D';
	}
	if (percent <= 0.70) {
		return 'C-';
	}
	if (percent <= 0.78) {
		return 'C';
	}
	if (percent <= 0.79) {
		return 'C+';
	}
	if (percent <= 0.8) {
		return 'B-';
	}
	if (percent <= 0.88) {
		return 'B';
	}
	if (percent <= 0.89) {
		return 'B+';
	}
	if (percent <= 0.90) {
		return 'A-';
	}
	if (percent <= 0.98) {
		return 'A';
	}

	return 'A+';
}

export function gradeToAllowance(grade) {
	const absolute = grade.match(/[A-F]/i).pop();
	return grades[absolute];
}

