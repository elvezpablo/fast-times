import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import Schedule from './components/Schedule.jsx';

/* global document */

const render = () => {
	ReactDOM.render(
		<AppContainer>
			<Schedule/>
		</AppContainer>,
    document.getElementById('schedule')
  );
};

render();

// Hot Module Replacement API
if (module.hot) {
	module.hot.accept('./components/Schedule.jsx', render);
}
