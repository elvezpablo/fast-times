
import request from 'superagent';

const URL = 'http://grades.rangelworks.com/grades.php';

class Grades {
	static load() {
		return new Promise((resolve, reject) => {
			request
        .get(URL)
        .end((err, res) => {
	if (err) {
		reject(err);
	}
	if (res) {
		resolve(res.body);
	}
});
		});
	}
}
export default Grades;
