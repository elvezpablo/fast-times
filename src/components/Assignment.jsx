import React, {PropTypes} from 'react';
import moment from 'moment';
import styles from '../css/Assignment.css';

const Assignment = props => {
	const className = [styles.assignment, 'clearfix'];
	const dueDate = moment(props.dueDate);
	const today = moment();

	if (parseInt(props.score, 10) === 0 && today.isAfter(dueDate)) {
		className.push(styles.missing);
	}
	if (props.score === '--') {
		className.push(styles.not_graded);
	}
	return (
		<div className={className.join(' ')}>
			{props.name}
			<div className={styles.info}>
				{dueDate.format('MM/DD')}
			</div>
			<div className={styles.info}>
				{parseInt(props.pointspossible, 10)}
			</div>
			<div className={styles.info}>
				{props.score}
			</div>
		</div>
	);
};

Assignment.propTypes = {
	score: PropTypes.string,
	name: PropTypes.string,
	pointspossible: PropTypes.string,
	dueDate: PropTypes.string,
};

export default Assignment;
