import React from 'react';
import styles from '../css/Loader.css';

const Loader = () => (<div className={styles.loader}/>);

export default Loader;
