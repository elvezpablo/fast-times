import React, {Component} from 'react';
import Grades from '../api/Grades';
import styles from '../css/Schedule.css';
import {computeLetterGrade, percentToLetterGrade, gradeToAllowance} from '../utils/GradeUtils';
import Period from './Period.jsx';
import Loader from './Loader.jsx';

function createPeriods(data, filtered) {
	return data.map((period, i) => {
		const grade = computeLetterGrade(period.assignments);
		let letterGrade = percentToLetterGrade(grade.percent);
		letterGrade = letterGrade || 'NA';
		const allowance = gradeToAllowance(letterGrade);

		return (<Period
			allowance={allowance}
			assignments={period.assignments}
			filtered={filtered}
			grade={grade}
			key={i}
			letterGrade={letterGrade}
			tardies={period.tardies}
			teacher={period.teacher}
			name={period.name}
			/>);
	});
}

class Schedule extends Component {

	constructor(props) {
		super(props);
		this.state = {
			btnShowAll: true,
			loading: true,
			periods: [],
		};
		this.handleClick = this.handleClick.bind(this);
	}

	componentDidMount() {
		Grades.load()
				.then(res => {
					this.setState({
						loading: false,
						periods: res,
					});
				})
				.catch(err => {
					console.error('Error loading grades: ', err);
				});
	}

	handleClick() {
		this.setState({btnShowAll: !this.state.btnShowAll});
	}
	render() {
		const {periods, btnShowAll, loading} = this.state;
		const btnLabel = (btnShowAll) ? 'Show All' : 'Show Important';

		return (<div>
			<div className={styles.controls}>
				<button className="button-primary" onClick={this.handleClick}>{btnLabel}</button>
			</div>
			{ loading ? <Loader/> : createPeriods(periods, btnShowAll)}
		</div>);
	}
}

export default Schedule;
