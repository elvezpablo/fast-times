
import React, {Component, PropTypes} from 'react';
import styles from '../css/Period.css';
import Assignment from './Assignment.jsx';
import Smile from './Smile.jsx';

class Period extends Component {

	getAssignments(assignments) {
		return assignments.map((assignment, index) => {
			if (this.props.filtered) {
				return (assignment.score === '--' || parseInt(assignment.score, 10) === 0) ? <Assignment key={index} {...assignment}/> : null;
			}
			return <Assignment key={index} {...assignment}/>;
		});
	}
	render() {
		const {
			assignments,
			allowance,
			grade,
			letterGrade,
			name,
			teacher,
			tardies,
		} = this.props;
		const pointsToC = (grade.pointsToC > 0) ? grade.pointsToC : <Smile/>;

		return (
			<div className={styles.class} >
				<div className="row">
					<div className="seven columns">
						<h3 className={styles.class_name}>{name}</h3>
						<h4 className={styles.teacher}>{teacher}</h4>
					</div>
					<div className={['five', 'columns', styles.relative].join(' ')}>
						<div className={styles.current_grade}> {pointsToC} </div>
						<div className={[styles.grade_summary, 'clearfix'].join(' ')}>
							<div className={styles.third}>
								{`Allowance: $${allowance}`}
							</div>
							<div className={styles.third}>
								{`Points: ${grade.total}/${grade.possible}`}
							</div>
							<div className={styles.third}>
								{`Grade: ${letterGrade}`}
							</div>
						</div>
						{ Boolean(tardies.length) && <h5 className={styles.tardies}>{tardies.length}</h5>}
					</div>
				</div>
				<div className="row">
					<div className={['twelve', 'columns', styles.assignments].join(' ')}>
						{this.getAssignments(assignments)}
					</div>
				</div>
			</div>
		);
	}
}

Period.propTypes = {
	allowance: PropTypes.number,
	assignments: PropTypes.array,
	filtered: PropTypes.bool,
	grade: PropTypes.object,
	letterGrade: PropTypes.string,
	name: PropTypes.string,
	tardies: PropTypes.array,
	teacher: PropTypes.string,
};

export default Period;
