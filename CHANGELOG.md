# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/elvezpablo/fast-times/compare/v1.3.0...v1.4.0) (2017-03-08)


### Features

* **component:** date parsing to top component ([6c7c0cd](https://gitlab.com/elvezpablo/fast-times/commit/6c7c0cd))
* **loader:** loading animation ([048f059](https://gitlab.com/elvezpablo/fast-times/commit/048f059))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/elvezpablo/fast-times/compare/v1.2.0...v1.3.0) (2017-03-07)


### Bug Fixes

* **Grades:** removing unused json ([e0f7607](https://gitlab.com/elvezpablo/fast-times/commit/e0f7607))


### Features

* **dates:** added school dates ([78ef75b](https://gitlab.com/elvezpablo/fast-times/commit/78ef75b))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/elvezpablo/fast-times/compare/v1.1.0...v1.2.0) (2017-03-04)


### Features

* **allowance:** added allowance to UI ([229b76b](https://gitlab.com/elvezpablo/fast-times/commit/229b76b))
* **data:** adding Dates data ([546154b](https://gitlab.com/elvezpablo/fast-times/commit/546154b))
* **gradeToAllowance:** letter grade to allowance ([ae88dfe](https://gitlab.com/elvezpablo/fast-times/commit/ae88dfe))



<a name="1.1.0"></a>
# 1.1.0 (2017-03-02)


### Features

* **import:** initial import ([893fb0f](https://gitlab.com/elvezpablo/fast-times/commit/893fb0f))
