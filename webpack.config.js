const {join} = require('path'); // eslint-disable-line  unicorn/filename-case
const webpack = require('webpack');

module.exports = {
	devtool: 'inline-source-map',
	entry: {
		main: [
			'react-hot-loader/patch',
			'webpack-dev-server/client?http://localhost:8080',
			'webpack/hot/only-dev-server',
			join(__dirname, 'src/main.jsx'),
		],
	},
	output: {
		filename: 'main.js',
		path: join(__dirname, '/js'),
		publicPath: '/js',
	},
	devServer: {
		hot: true,
		contentBase: __dirname,
		publicPath: '/js',
		stats: {
			colors: true,
			hash: false,
			version: false,
			timings: false,
			assets: false,
			chunks: false,
			modules: false,
			reasons: false,
			children: false,
			source: false,
			errors: false,
			errorDetails: false,
			warnings: false,
			publicPath: false,
		},
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('development'),
		}),
	],
	performance: {
		hints: false,
	},
	module: {
		rules: [
			{
				test: /\.json$/,
				use: [{loader: 'json-loader'}],
			},
			{
				test: /\.js$|\.jsx$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: [
								[
									'es2015',
									{
										modules: false,
									},
								],
								'stage-2',
								'react',
							],
							plugins: ['react-hot-loader/babel'],
						},
					},
				],
			},
			{
				test: /\.css$/,
				use: [
					{loader: 'style-loader'},
					{loader: 'css-loader?modules'},
				],
			},
		],
	},
};
