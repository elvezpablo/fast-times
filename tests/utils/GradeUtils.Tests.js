import test from 'ava';
import {computeLetterGrade, gradeToAllowance, percentToLetterGrade} from '../../src/utils/GradeUtils';

test('computeLetterGrade', t => {
	const assignments = [
		{
			score: 5,
			pointspossible: 10,
		},
		{
			score: 5,
			pointspossible: 10,
		},
	];

	const expected = {
		percent: 0.5,
		pointsToC: 4,
		possible: 20,
		total: 10,
	};

	t.deepEqual(computeLetterGrade(assignments), expected, 'summary not expected');
});

test('percentToLetterGrade', t => {
	t.is(percentToLetterGrade(0.58), 'F', '0.58 should be F');
});

test('gradeToAllowance ', t => {
	t.is(gradeToAllowance('A-'), 5, 'A should be $5');
	t.is(gradeToAllowance('B-'), 4, 'A should be $4');
	t.is(gradeToAllowance('C-'), 3, 'A should be $3');
});

