// import tape from 'blue-tape';
// // const GradeUtils = require('../src/utils/GradeUtils');
// import { percentToLetterGrade } from '../src/utils/GradeUtils';
//
// tape('a test was run', (t) => {
//   t.equal(percentToLetterGrade(0.9), 'A-', '.9 is A-');
//   t.equal(percentToLetterGrade(0.59), 'F+', '.59 is F');
//   t.equal(percentToLetterGrade(0.2), 'F', '.2 is F');
//   t.end();
// });

import test from 'ava';

test('aTest', t => {
	t.is(1, 1, 'Should be one');
});

test('a new Test', t => {
	t.is(2, 2, 'Should be two');
});
